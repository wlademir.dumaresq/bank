package br.com.lista;

public class ContaCorrente extends ContaBancaria {

    private double operationalTax;

    public ContaCorrente (String nomeTitular, String cpfTitular){
        super(nomeTitular, cpfTitular);
        this.saudo = 0;
        this.operationalTax = 0;
    }

    public ContaCorrente (String nomeTitular, String cpfTitular, boolean status){
        super(nomeTitular, cpfTitular, status);
        this.saudo = 0;
        this.operationalTax = 0;
    }
    public ContaCorrente(int idaccount, double saudo, double operationalTax) {
        super(idaccount, saudo);
        this.operationalTax = operationalTax;
    }

    @Override
    public boolean sacar(double saque) {

        if(this.getSaudo() - saque - operationalTax < 0){
            return false;
        }

        this.setSaudo(this.saudo - saque - operationalTax);
        return true;
    }

    @Override
    public boolean depositar(double deposit) {
        if (this.getSaudo() + deposit - operationalTax < 0) {
            return false;
        }

        this.setSaudo(this.saudo + deposit - operationalTax);
        return true;
    }

    public double getOperationalTax() {
        return operationalTax;
    }

    @Override
    public String mostrarDados() {

            StringBuffer stringData = new StringBuffer();
            stringData.append(super.mostraDados()).append("Operationa Tax  = ").append(this.getOperationalTax()).append("\n");
            return stringData.toString();
    }

    @Override
    public boolean transfer(double value, ContaBancaria account){
        if(this.sacar(value)){
            //Case Duplicate operationalTax
            if(account.getClass() == this.getClass()){
                return account.depositar(value + operationalTax);
            }

            return account.depositar(value);

        } else{
            return false;
        }

    }

    @Override
    public boolean equals(Object obj) {

        if(this == obj){
            return true;
        }

        if(obj == null || this.getClass() != obj.getClass()){
            return false;
        }

        ContaCorrente contaCorrente = (ContaCorrente) obj;
        return contaCorrente.getIdaccount() == this.getIdaccount();
    }

    @Override
    public boolean equals(int idaccount) {
        ContaCorrente contaCorrente = new ContaCorrente(idaccount,0,0);
        return this.equals(contaCorrente);
    }

    @Override
    public int hashCode() {
        return this.getIdaccount();
    }
}
