package br.com.lista;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {

        Banco bank = new Banco();

        bank.criarContaCorrente("wlademir", "700.726.254-65");
        bank.criarContaCorrente("Andre", "113.447.843-81");
        bank.criarContaCorrente("Martin", "-1",false);
        bank.criarContaCorrente("Martin", "123123123");
        bank.criarContaCorrente("Jordeva", "3");
        bank.criarContaCorrente("wlademir", "700.726.254-65", false);

        CompareContaToNome nome = new CompareContaToNome();
        CompareStatus status = new CompareStatus();
        CompareContaToCPF cpf = new CompareContaToCPF();
        CompareNameandStatus nameStatus = new CompareNameandStatus();
        CompareNameAndCPF nameCpf = new CompareNameAndCPF();
        CompareCPFTitularandStatus cpfstatus = new CompareCPFTitularandStatus();

        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("////////////////////COMPARE POR NOME /////////////////////////");
        System.out.println("//////////////////////////////////////////////////////////////");
        Collections.sort(bank.getContasData(), nome);
        System.out.println(bank.mostrarDados());
        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("////////////////////COMPARE POR STATUS////////////////////////");
        System.out.println("//////////////////////////////////////////////////////////////");
        Collections.sort(bank.getContasData(), status);
        System.out.println(bank.mostrarDados());
        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("////////////////////COMPARE POR CPF///////////////////////");
        System.out.println("//////////////////////////////////////////////////////////////");
        Collections.sort(bank.getContasData(), cpf);
        System.out.println(bank.mostrarDados());
        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("////////////////////COMPARE POR NAMESTATUS////////////////////");
        System.out.println("//////////////////////////////////////////////////////////////");
        Collections.sort(bank.getContasData(), nameStatus);
        System.out.println(bank.mostrarDados());
        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("////////////////////COMPARE POR NAMECPF///////////////////////");
        System.out.println("//////////////////////////////////////////////////////////////");
        Collections.sort(bank.getContasData(), nameCpf);
        System.out.println(bank.mostrarDados());

        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("////////////////////COMPARE POR CPFSTATUS///////////////////////");
        System.out.println("//////////////////////////////////////////////////////////////");

        Collections.sort(bank.getContasData(), cpfstatus);
        System.out.println(bank.mostrarDados());


        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("////////////////////SEARCH POR CPF///////////////////////");
        System.out.println("//////////////////////////////////////////////////////////////");
        Banco banco = new Banco(bank.procurarContaPorCPF("700.726.254-65"));
        System.out.println(banco.mostrarDados());

        System.out.println("//////////////////////////////////////////////////////////////");
        System.out.println("////////////////////SEARCH POR NOME///////////////////////");
        System.out.println("//////////////////////////////////////////////////////////////");
        banco = new Banco(bank.procurarContaPorTitular("Martin"));
        System.out.println(banco.mostrarDados());


/*


//        ContaCorrente contaCorrente = new ContaCorrente(1,300.0, 4.90);
//        ContaPoupanca contaPoupanca = new ContaPoupanca(2,300.0, 400);

        System.out.println("Saque realizado ? " + contaCorrente.sacar(300));

        Relatorio relatorio = new Relatorio();

        System.out.println("Transferencia realizada ? " + contaCorrente.transfer(30, contaPoupanca));

        System.out.println("Relatorio da conta corrente : \n" + relatorio.gerarRelatorio(contaCorrente));

        System.out.println("Relatorio da conta Poupanca : \n" + relatorio.gerarRelatorio(contaPoupanca));

 */
/*
        Banco b = new Banco();
        System.out.println("Insert ContaCorrente :" + b.insert(contaCorrente));
        System.out.println("Insert ContaCorrente :" + b.insert(contaCorrente));

        System.out.println("Insert ContaPoupança :" + b.insert(contaPoupanca));
        System.out.println("Insert ContaPoupança :" + b.insert(contaPoupanca));


        System.out.println("Tamanho do banco : " + b.size());
        System.out.println(b.mostrarDados());

        System.out.println("Remoção funcionando ? " + b.remove(contaCorrente));
        System.out.println("Remoção não existe ? " + b.remove(contaCorrente));


        System.out.println("Remoção com numero funcionando ? " + b.remove(2));

        System.out.println("Tamanho do banco : " + b.size());
        System.out.println(b.mostrarDados());

        System.out.println(b.procurarConta(contaCorrente.getIdaccount()));

*/
    }
}
