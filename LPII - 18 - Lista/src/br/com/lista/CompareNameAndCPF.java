package br.com.lista;

import java.util.Comparator;

public class CompareNameAndCPF implements Comparator<ContaBancaria> {

    public int compare(ContaBancaria obj1, ContaBancaria obj2) {
        CompareContaToNome c1 = new CompareContaToNome();

        if(c1.compare(obj1, obj2) == 0 ) {
            CompareContaToCPF c2 = new CompareContaToCPF();
            return c2.compare(obj1, obj2);
        }
        return c1.compare(obj1,obj2);
    }
}
