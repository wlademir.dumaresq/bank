package br.com.lista;

import java.util.Comparator;

public class CompareContaToNome implements Comparator<ContaBancaria> {
    @Override
    public int compare (ContaBancaria obj1, ContaBancaria obj2) {
        return obj1.getNomeTitular().compareTo(obj2.getNomeTitular());
    }
}
