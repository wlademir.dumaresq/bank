package br.com.lista;

import java.util.Comparator;

public class CompareContaToCPF implements Comparator<ContaBancaria> {
    @Override
    public int compare (ContaBancaria obj1, ContaBancaria obj2) {
        return obj1.getCPFTitular().compareTo(obj2.getCPFTitular());
    }
}
