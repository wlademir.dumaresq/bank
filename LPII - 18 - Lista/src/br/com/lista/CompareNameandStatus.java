package br.com.lista;

import java.util.Comparator;

public class CompareNameandStatus implements Comparator<ContaBancaria> {
    public int compare(ContaBancaria obj1, ContaBancaria obj2) {
        CompareContaToNome comp1 = new CompareContaToNome();

        if (comp1.compare(obj1, obj2) == 0) {
            CompareStatus comp2 = new CompareStatus();

            return comp2.compare(obj1, obj2);
        }

        return comp1.compare(obj1, obj2);
    }
}

