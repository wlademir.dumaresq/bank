package br.com.lista;

import javax.xml.crypto.*;
import java.time.*;

public abstract class ContaBancaria implements Imprimivel{
    protected String nomeTitular;
    protected String CPFTitular;
    protected LocalDate dataCadastro;
    protected Data dataEncerramento;
    protected boolean ativa;
    protected int Idaccount;
    protected double saudo;


    public ContaBancaria(String nomeTitular, String CPFTitular) {
        this.nomeTitular = nomeTitular;
        this.CPFTitular = CPFTitular;
        this.ativa = true;
        this.dataCadastro = LocalDate.now();
        this.dataEncerramento = null;
    }

    public ContaBancaria(String nomeTitular, String CPFTitular, boolean status) {
        this.nomeTitular = nomeTitular;
        this.CPFTitular = CPFTitular;
        this.ativa = status;
        this.dataCadastro = LocalDate.now();
        this.dataEncerramento = null;
    }

    public ContaBancaria(int idaccount, double saudo) {
        Idaccount = idaccount;
        this.saudo = saudo;
    }

    public abstract boolean sacar(double saque);
    public abstract boolean depositar(double deposit);

    //Getters


    public boolean isAtiva() {
        return ativa;
    }

    public String getNomeTitular() {
        return nomeTitular;
    }

    public String getCPFTitular() {
        return CPFTitular;
    }

    public int getIdaccount() {
        return Idaccount;
    }

    public double getSaudo() {
        return saudo;
    }



    //Setters
    protected void setSaudo(double saudo) {
        this.saudo = saudo;
    }

    protected void setIdaccount(int idaccount) {
        this.Idaccount = idaccount;
    }

    public void setAtiva(boolean ativa) {
        this.ativa = ativa;
    }

    //Methods

    public String mostraDados() {
        StringBuffer str = new StringBuffer();
        str.append("Nome do Titular " + this.nomeTitular + "\n"
                + "Cpf do Titular : " + this.CPFTitular + "\n"
                + "Data de cadastro : " + this.dataCadastro + "\n");

        if (this.ativa){
            str.append("Situação da conta : Conta ativa\n");
        }else{
            str.append("Situação da conta : Conta desativada\n");
            str.append("Data de encerramento : " + this.dataEncerramento + "\n");
        }

        str.append("Idaccount = " + this.getIdaccount() + "\n"
                +"Saudo = " + this.getSaudo() +"\n");

        return str.toString();
    }

    public abstract boolean transfer ( double value, ContaBancaria account);

    public abstract boolean equals(int idaccont);


}
