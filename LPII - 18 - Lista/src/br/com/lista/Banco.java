package br.com.lista;

import java.util.ArrayList;

public class Banco implements Imprimivel {
    ArrayList <ContaBancaria> contasData;


    public Banco(){
        contasData = new ArrayList<ContaBancaria>();
    }

    public Banco(ArrayList<ContaBancaria> contas){
        contasData = contas;
    }

    public boolean insert(ContaBancaria obj){
            this.contasData.add(obj);
            return true;
    }

    public ArrayList<ContaBancaria> getContasData() {
        return contasData;
    }

    public boolean remove(ContaBancaria obj){
        return this.contasData.remove(obj);
    }

    public boolean remove(int idaccont){
        return this.remove(this.procurarConta(idaccont));
    }

    public boolean haveAccount (int idaccount){

        if( contasData.isEmpty()){
            return false;
        }

        for (ContaBancaria a : contasData) {
            if( a.equals(idaccount)){
                return true;
            }
        }
        return false;
    }


    public void criarContaCorrente(String nomeTitular, String cpfTitular, boolean status) {
        ContaCorrente conta = new ContaCorrente(nomeTitular, cpfTitular, status);
        insert(conta);
    }

    public void criarContaCorrente(String nomeTitular, String cpfTitular) {
        ContaCorrente conta = new ContaCorrente(nomeTitular, cpfTitular);
        insert(conta);
    }

    public void criarContaPoupanca(String nomeTitular, String cpfTitular, boolean status) {
        ContaPoupanca conta = new ContaPoupanca(nomeTitular, cpfTitular, status);
        insert(conta);
    }

    public ContaBancaria procurarConta (int idaccount){

        if( contasData.isEmpty()){
            return null;
        }

        for (ContaBancaria a : contasData) {
            if( a.equals(idaccount)){
                return a;
            }
        }
        return null;
    }

    public ArrayList<ContaBancaria> procurarContaPorTitular ( String titular){
        ArrayList<ContaBancaria> aux = new ArrayList<ContaBancaria>();

        if( contasData.isEmpty()){
            return null;
        }

        for (ContaBancaria a : contasData) {
            if( a.getNomeTitular().equals(titular)){
                aux.add(a);
            }
        }

        return aux;
    }

    public ArrayList<ContaBancaria> procurarContaPorCPF ( String cpf){
        ArrayList<ContaBancaria> aux = new ArrayList<ContaBancaria>();

        if( contasData.isEmpty()){
            return null;
        }

        for (ContaBancaria a : contasData) {
            if( a.getCPFTitular().equals(cpf)){
                aux.add(a);
            }
        }

        return aux;
    }

    @Override
    public String mostrarDados() {
        StringBuffer str = new StringBuffer();

        for(ContaBancaria it : contasData){
            str.append(it.mostrarDados());
            str.append("\n");
        }
        return str.toString();
    }

    public int size(){
        return contasData.size();
    }



}
