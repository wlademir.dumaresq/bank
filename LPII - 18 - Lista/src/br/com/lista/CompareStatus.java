package br.com.lista;

import java.util.Comparator;

public class CompareStatus implements Comparator<ContaBancaria> {
    public int compare (ContaBancaria obj1, ContaBancaria obj2) {
        if ( obj1.isAtiva() == obj2.isAtiva()){
            return 1;
        }else if ( obj1.isAtiva() && !obj2.isAtiva()){
            return 0;
        }else {
            return 1;
        }
    }
}
