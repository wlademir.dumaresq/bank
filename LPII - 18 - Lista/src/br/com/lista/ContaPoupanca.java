package br.com.lista;

public class ContaPoupanca extends ContaBancaria{

    private int limit;


    public ContaPoupanca (String nomeTitular, String cpfTitular, boolean status){
        super(nomeTitular, cpfTitular, status);
        this.saudo = 0;
        this.limit = 0;
    }

    public ContaPoupanca (String nomeTitular, String cpfTitular){
        super(nomeTitular, cpfTitular);
        this.saudo = 0;
        this.limit = 0;
    }

    public ContaPoupanca(int idaccount, double saudo, int limit) {
        super(idaccount, saudo);
        this.limit = limit;
    }

    @Override
    public boolean sacar(double saque) {
        if((Math.abs(this.getSaudo() - saque) < limit)
                && ((this.getSaudo() - saque) < 0))
        {
            return false;
        }

        this.setSaudo(this.saudo - saque);
        return true;
    }

    @Override
    public boolean depositar(double deposit) {
        if (deposit < 0){
            System.out.println("Erro deposit negative value");
            return false;
        }
        this.setSaudo(this.getSaudo() + deposit);
        return true;
    }

    public int getLimit() {
        return limit;
    }

    public String mostrarDados() {
       StringBuffer stringData = new StringBuffer();
       stringData.append(super.mostraDados()).append("Limit  = ").append(this.getLimit()).append("\n");
        return stringData.toString();
    }

    public boolean transfer(double value, ContaBancaria account) {
        if(this.sacar(value)){
            return account.depositar(value);
        } else{
            return false;
        }
    }

    @Override
    public boolean equals(Object obj) {

        if(this == obj){
            return true;
        }

        if(obj == null || this.getClass() != obj.getClass()){
            return false;
        }

        ContaPoupanca poupanca = (ContaPoupanca) obj;
        return poupanca.getIdaccount() == this.getIdaccount();
    }

    @Override
    public boolean equals(int idaccount) {
        ContaPoupanca poupanca = new ContaPoupanca(idaccount, 0 ,0);
        return this.equals(poupanca);
    }

    @Override
    public int hashCode() {
        return this.getIdaccount();
    }
}
